<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Liste des fournisseurs</title>
        <link href="../css/style.css" rel="stylesheet">
        <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Les fournisseurs</h1>

<?php
include("../include/connexion.php");

/**
 * Page qui affiche la liste de tout les fournisseurs
 */

 ;

$requete = 'SELECT c.libelle
            , f.nom
            , f.contact
            , f.code
            , v.codepostal
            , v.nom as ville
            FROM fournisseur f
            , civilite c
            , ville v
            where f.civilite = c.code
            and f.ville = v.code';

        $resultat = $bdd->query($requete);

?>

<table class="table table-striped display">
  
<thead>
        <tr>
            <th>Nom</th>
            <th>Civilité</th>
            <th>Contact</th>
            <th>Code Postal</th>
            <th>Ville</th>
        </tr>
    </thead>  
 <tbody>  

<?php
   
try {
    foreach($resultat as $ligne) {
        echo '<tr class="clickable-row" data-href="fournisseur.php?id='.$ligne['code'].'">';
        echo '<td>' . $ligne['nom'] . '</td>';
        echo '<td>' . $ligne['libelle'] . '</td>' ;
        echo '<td>' . $ligne['contact'] . '</td>';
        echo '<td>'. $ligne['codepostal'] . '</td> ' ;
        echo '<td>'. $ligne['ville'] . "</td>\n";
    }
} catch (PDOException $e) {
    echo 'Erreur !: ' . $e->getMessage() . '<br>';
    die();
}
?>
</tr>
</tbody>
</div>
</table>
        
<script src="../node_modules/jquery/dist/jquery.min.js"></script>
        <script src="../node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../node_modules/dataTables.net-bs5/js/dataTables.bootstrap5.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#villes').DataTable({
                    language: {
                        url: '../include/fr_FR.json'
                    }
            });
            });
            </script>

<script src="js/jquery.js"></script>
<script>
    $(document).ready(function($) {
        $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
    });
</script>
   

 </body>
</html>